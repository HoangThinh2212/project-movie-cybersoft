import React from "react";
import { NavLink } from "react-router-dom";

export default function FilmCardMobile({ film }) {
  return (
    <div className="flex items-center bg-gray-800 border rounded-lg shadow-md hover:bg-gray-100 p-0">
      {/*card Left */}
      <div className="card-left w-2/5">
        <img
          className="object-cover rounded-t-lg h-72 w-full"
          src={film.hinhAnh}
          alt="true"
        />
      </div>

      {/*card  Right */}
      <div className="card-right w-3/5">
        <div className="p-5 leading-normal">
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-white dark:text-white">
            {film.tenPhim}
          </h5>
          <p className="mb-3 font-normal text-gray-400 dark:text-gray-300">
            {film.moTa.length > 100 ? film.moTa.slice(0, 100) : film.moTa}
          </p>
          <NavLink
            to={`/detail/${film.maPhim}`}
            className="inline-flex items-center mt-4 px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300"
          >
            Detail
            <svg
              aria-hidden="true"
              className="w-4 h-4 ml-2 -mr-1"
              fill="currentColor"
              viewBox="0 0 20 20"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fill-rule="evenodd"
                d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                clip-rule="evenodd"
              ></path>
            </svg>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
