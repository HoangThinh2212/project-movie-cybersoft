import React from "react";
import { NavLink } from "react-router-dom";

export default function FilmCardTablet({ film }) {
  return (
    <div
      className="max-w-sm rounded-lg shadow-md bg-blue-400 border-black"
      style={{ height: "500px" }}
    >
      <NavLink to={`/detail/${film.maPhim}`} href="#">
        <img
          className="rounded-t-lg h-72 object-cover w-full"
          src={film.hinhAnh}
          alt=""
        />
      </NavLink>
      <div className="p-5">
        <NavLink to={`/detail/${film.maPhim}`} href="#">
          <h5 className="mb-2 text-xl font-bold text-white h-12">
            {film.tenPhim}
          </h5>
        </NavLink>
        <p className="mb-3 font-normal text-white h-14">
          {film.moTa.length > 40 ? film.moTa.slice(0, 40) : film.moTa}
        </p>
        <NavLink
          to={`/detail/${film.maPhim}`}
          className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-pink-600 rounded-lg hover:bg-yellow-300"
        >
          Xem chi tiết
          <svg
            aria-hidden="true"
            className="w-4 h-4 ml-2 -mr-1"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fill-rule="evenodd"
              d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
              clip-rule="evenodd"
            ></path>
          </svg>
        </NavLink>
      </div>
    </div>
  );
}
