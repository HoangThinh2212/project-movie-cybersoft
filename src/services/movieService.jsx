import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";
// coi chừng sai => check lại bên clip sỹ

export const movieService = {
  getDanhSachPhim: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP03`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getPhimTheoHeThongRap: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP03`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getChiTietPhim: (id) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`,
      method: "GET",
      headers: createConfig(),
    });
  },
}
