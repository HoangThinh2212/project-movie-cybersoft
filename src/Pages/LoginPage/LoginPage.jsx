import React from "react";
import { Button, Form, Input, message } from "antd";
import { userService } from "../../services/userService";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFOR } from "../../redux/constant/userConstant";
import { userLocalStorage } from "../../services/localStorage";
import bg_animate from "../../assets/not-found.json";
import Lottie from "lottie-react";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    userService
      .postDangNhap(values)
      .then((res) => {
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });

        // Lưu vào localStorage
        userLocalStorage.set(res.data.content);

        message.success("Login successful!");
        setTimeout(() => {
          navigate("/");
        }, 1000);
      })
      .catch((err) => {
        message.error("Login failed");
        console.log(err);
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 lg:flex lg:justify-center lg:items-center">
        <div className=" px-5 h-full lg:w-1/2 md:w-full">
          <Lottie animationData={bg_animate} loop={true} />
        </div>
        <div className="h-full lg:w-1/2 md:w-full bg-pink-200 px-10 py-10 rounded-lg md:mt-10 sm:mt-10">
          <div className="text-xl text-red-500 mb-5 font-bold">
            Login or Register
          </div>
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              className="font-bold text-lg"
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  message: "Nhập username vô thằng quần!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              className="font-bold text-lg"
              label="Password"
              name="matKhau"
              rules={[
                {
                  message: "Password đâu thằng quỷ ?",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <NavLink to="/signup">
              <span className="font-bold text-md text-red-500 mb-4 hover:text-white">
                *Sign up for free now
              </span>
            </NavLink>
            <Form.Item
              wrapperCol={{
                span: 24,
              }}
              className="text-center"
            >
              <Button
                type="primary"
                htmlType="submit"
                className="bg-red-500 text-white hover:bg-white hover:text-red-500"
              >
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
