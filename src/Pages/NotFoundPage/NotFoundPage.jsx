import React from "react";
import Lottie from "lottie-react";
import bg_animate from "../../assets/404.json";

export default function NotFoundPage() {
  return (
    <div className="flex justify-center items-center h-screen w-screen">
      <Lottie
          animationData={bg_animate}
          loop={true}
          style={{ width: "70%", margin: "auto"}}
        />
    </div>
  );
}
