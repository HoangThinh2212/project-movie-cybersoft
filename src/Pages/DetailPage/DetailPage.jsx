import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import moment from "moment";
import { Rate } from "antd";
import { Tabs } from "antd";
import { Progress } from "antd";
import "../../../node_modules/react-modal-video/scss/modal-video.scss";
import ModalVideo from "react-modal-video";
import { PlayCircleFilled } from "@ant-design/icons";

export default function DetailPage() {
  const [movie, setMovie] = useState([]);

  const [isOpen, setOpen] = useState(false);

  const { id } = useParams();

  const onChange = (key) => {
    console.log(key);
  };

  const getYoutubeVideoId = (url) => {
    if (url) {
      url = url.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
      return url[2] !== undefined ? url[2].split(/[^0-9a-z_\-]/i)[0] : url[0];
    }
  };

  useEffect(() => {
    movieService
      .getChiTietPhim(id)
      .then((res) => {
        console.log(res.data.content);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);

  const renderRap = () => {
    return movie.heThongRapChieu?.map((rap) => {
      return {
        label: (
          <div className='flex items-center '>
            <img src={rap.logo} className='w-20 h-20' alt='true' />
            <p className='ml-3 text-lg'>{rap.maHeThongRap}</p>
          </div>
        ),
        key: rap.maHeThongRap,
        children: rap.cumRapChieu.map((phim) => {
          return (
            <div className='pb-5'>
              <div className='flex'>
                <img className='w-20 h-20' src={phim.hinhAnh} alt='true' />
                <div className='ml-3 text-xl '>
                  <div className='text-bold text-red-700'>
                    {phim.tenCumRap}
                  </div>
                  <div className='pt-2 text-sm text-lighter'>{phim.diaChi}</div>
                </div>
              </div>
              <div className='grid pt-3 gap-5 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1'>
                {phim.lichChieuPhim?.slice(0, 10).map((lich) => {
                  return (
                    <div className='flex text-md p-3 border rounded border-gray-100 justify-self-start'>
                      <div className='text-green-500'>
                        {moment(lich.ngayChieuGioChieu).format("DD-MM-YYYY")}
                      </div>
                      ~
                      <div className='text-red-500'>
                        {moment(lich.ngayChieuGioChieu).format("HH:MM")}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          );
        }),
      };
    });
  };

  return (
    <div
      className='mx-auto'
      style={{
        backgroundColor: "#D553CF",
      }}
    >
      <ModalVideo
        channel='youtube'
        autoplay
        isOpen={isOpen}
        videoId={getYoutubeVideoId(movie.trailer)}
        onClose={() => setOpen(false)}
      />
      <div
        style={{
          padding: "100px 0",
        }}
        className='text-white grid xl:grid-cols-3 lg:grid-cols-3 md:grid-cols-1 sm:grid-cols-1 gap-5 '
      >
        <div
          className='relative overflow-hidden h-96 w-64 xl:justify-self-end sm:justify-self-center'
          data-mdb-ripple='true'
          data-mdb-ripple-color='light'
        >
          <img
            src={movie.hinhAnh}
            alt='true'
            className='h-96 w-64 xl:mr-5 lg:mr-5 md:mr-5 sm:mx-auto'
          />

          <div
            className='absolute top-0 right-0 bottom-0 left-0 w-full h-full overflow-hidden bg-fixed opacity-0 hover:opacity-100 transition duration-300 ease-in-out lg:justify-self-end'
            style={{ backgroundColor: "rgba(251, 251, 251, 0.2)" }}
          >
            <PlayCircleFilled
              onClick={() => setOpen(true)}
              className='text-5xl absolute top-1/2 right-1/2 translate-x-1/2 -translate-y-1/2'
            />
          </div>
        </div>

        <div className='flex flex-col xl:place-content-center xl:justify-self-start lg:place-content-center lg:justify-self-start md:place-content-center md:justify-self-center sm:place-content-center sm:justify-self-center'>
          <div>{moment(movie.ngayKhoiChieu).format("DD.MM.YYYY")}</div>
          <div className='text-2xl'>{movie.tenPhim}</div>
        </div>

        <div className='flex flex-col xl:place-content-center xl:justify-self-start lg:place-content-center lg:justify-self-start md:place-content-center md:justify-self-center sm:place-content-center sm:justify-self-center'>
          <Progress
            type='circle'
            percent={movie.danhGia * 10}
            format={() => `${movie.danhGia}`}
          />
          <Rate allowHalf disabled value={movie.danhGia / 2} />
        </div>
      </div>

      <div style={{ padding: "50px 0" }} className='w-4/5 mx-auto bg-white '>
        <Tabs
          tabPosition='left'
          defaultActiveKey='1'
          onChange={onChange}
          items={renderRap()}
        />
      </div>
    </div>
  );
}
