import moment from "moment/moment";
import React from "react";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3 h-64">
      <img
        className="w-40 h-45 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt="true"
      />
      <div>
        <h5 className="text-2xl mb-8 font-semibold">{movie.tenPhim}</h5>
        <div className="grid grid-cols-2 gap-4">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((item, index) => {
            return (
              <span key={index} className="p-2 bg-pink-500 text-white rounded">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YY - hh:mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
