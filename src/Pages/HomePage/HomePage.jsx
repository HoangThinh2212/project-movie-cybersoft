import React, { useEffect, useState } from "react";
import Footer from "../../Components/Footer/Footer";
import FilmList from "../../Components/FilmList/FilmList";
import HomeCarousel from "../../Components/HomeCarousel/HomeCarousel";
import { movieService } from "../../services/movieService";
import MovieTabs from "./MovieTabs/MovieTabs";
import Blogs from "./Blogs/Blogs";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <HomeCarousel />
      <div className="container mx-auto">
        <FilmList movieArr={movieArr} />
        <MovieTabs />
        <Blogs />
      </div>
      <Footer />
    </div>
  );
}
