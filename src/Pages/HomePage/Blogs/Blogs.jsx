import React from "react";
import { NavLink } from "react-router-dom";

export default function Blogs() {
  return (
    <div className="blogs mt-20">
      <div className="mb-3 border-b border-pink-600">
        <ul
          className="flex flex-wrap -mb-px font-medium text-center justify-center items-center"
          id="myTab"
          data-tabs-toggle="#myTabContent"
          role="tablist"
        >
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg text-2xl text-pink-600"
              id="profile-tab"
              data-tabs-target="#profile"
              type="button"
              role="tab"
              aria-controls="profile"
              aria-selected="false"
            >
              Phim 24h
            </button>
          </li>
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg hover:text-pink-600 hover:border-pink-600 text-2xl text-pink-600"
              id="dashboard-tab"
              data-tabs-target="#dashboard"
              type="button"
              role="tab"
              aria-controls="dashboard"
              aria-selected="false"
            >
              Review
            </button>
          </li>
          <li className="mr-2" role="presentation">
            <button
              className="inline-block p-4 border-b-2 rounded-t-lg hover:text-pink-600 hover:border-pink-600 text-2xl text-pink-600"
              id="settings-tab"
              data-tabs-target="#settings"
              type="button"
              role="tab"
              aria-controls="settings"
              aria-selected="false"
            >
              Khuyến mãi
            </button>
          </li>
        </ul>
      </div>

      <div id="myTabContent">
        {/* Tab 1 */}
        <div
          className="p-4 rounded-lg grid lg:grid-cols-3 lg:gap-4 text-justify"
          id="profile"
          role="tabpanel"
          aria-labelledby="profile-tab"
        >
          <div>
            <img
              src="https://media.tenor.com/WmMc1JH4_TkAAAAd/coffin-meme-coffin-dance.gif"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-bold mt-2 hover:text-yellow-400 cursor-pointer">
                Project Movie không phải là Final
              </p>
            </NavLink>
            <p className="text-blue-500 text-sm mt-5 sm:mb-8">
              Mr Tâm và Thịnh bị bamboozled - their disapointment is imersuarable and their days is ruined !!!
              Tất cả là tại thằng Thịnh
            </p>
          </div>
          <div>
            <img
              src="https://www.animenewsnetwork.com/thumbnails/crop1200x630gC7/cms/news.4/167200/slowahead.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl text-pink-500 font-bold mt-2 hover:text-red-500 cursor-pointer">
                Azur Lane Slow Ahead
              </p>
            </NavLink>
            <p className="text-blue-500 text-sm mt-5 sm:mb-8">
              Anime hay nhất của năm - dành cho thư giãn giải trí và những ai đam mê world of war ships
            </p>
          </div>
          <div>
            <img
              src="https://gora.vn/wp-content/uploads/2017/08/Cyber-1.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl text-yellow-300 font-bold mt-2 hover:text-red-500 cursor-pointer">
                Học Cybersoft thay đổi sự nghiệp
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5">
              Ngành IT Việt Nam hiện nay ở đầu của sự phát triển. Học IT tuy khó nhưng đáng,
              lương ổn định, không phải đi nhậu nếu bạn không thích nhậu, thiên hạ cả nể tưởng mình là Hacker man
            </p>
          </div>
        </div>

        {/* Tab 2 */}
        <div
          className="p-4 rounded-lg grid lg:grid-cols-3 lg:gap-4 text-justify"
          id="dashboard"
          role="tabpanel"
          aria-labelledby="dashboard-tab"
        >
          <div>
            <img
              src="https://i.ytimg.com/vi/-y01G4KZ6v8/maxresdefault.jpg"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl text-blue-700 font-bold mt-2 hover:text-red-500 cursor-pointer">
                Chơi Fate Grand Order
              </p>
            </NavLink>
            <p className="text-black-500 text-sm mt-5 sm:mb-8">
              Hậu quả khôn lường của một thanh niên cắm mặt vô động muối
            </p>
          </div>
          <div>
            <img
              src="https://i.gifer.com/Jdcb.gif"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl text-red-800 font-bold mt-2 hover:text-red-500 cursor-pointer">
                "Anh Tâm ơi Project tới đâu rồi" - Thịnh trước khi bị bóp cổ tới chết
              </p>
            </NavLink>
            <p className="text-pink-500 text-sm mt-5 sm:mb-8">
              Một thanh niên hỏi thăm dù ý tốt nhưng sai thời điểm...hậu quả khôn lường.
            </p>
          </div>
          <div>
            <img
              src="https://media.tenor.com/BjBPy1LUfr0AAAAC/akairo-azur-lane.gif"
              alt=""
              className="w-full object-cover rounded"
            />
            <NavLink to={"/tintuc"}>
              <p className="text-xl font-semibold mt-2 hover:text-red-500 cursor-pointer">
                Cyka Blyat
              </p>
            </NavLink>
            <p className="text-gray-500 text-sm mt-5">
              Xong project rồi thì làm gì ? Nhậu thôi !!!
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
