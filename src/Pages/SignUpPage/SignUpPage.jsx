import React from "react";
import { useFormik } from "formik";
import { userService } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFOR } from "../../redux/constant/userConstant";
import { userLocalStorage } from "../../services/localStorage";
import { message } from "antd";

export default function SignUpPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const formik = useFormik({
    initialValues: {
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDt: "",
      maNhom: "",
      hoTen: "",
    },
    onSubmit: (values) => {
      userService
        .postDangKy(values)
        .then((res) => {
          dispatch({
            type: SET_USER_INFOR,
            payload: res.data.content,
          });
          userLocalStorage.set(res.data.content);
          message.success("Sign up successful!");
          console.log(res);

          setTimeout(() => {
            navigate("/");
          }, 1000);
        })
        .catch((err) => {
          console.log(`err: `, err);
        });
      console.log(`values: `, values);
    },
  });

  return (
    <form className="bg-red-500" onSubmit={formik.handleSubmit}>
      <div className="flex justify-center min-h-screen">
        <div
          className="hidden bg-cover lg:block lg:w-2/5"
          style={{
            backgroundImage:
              "url(https://i.kym-cdn.com/photos/images/facebook/001/687/487/ba3.jpg)",
            backgroundPosition: "center",
            backgroundSize: "cover",
          }}
        ></div>
        <div className="flex items-center w-full max-w-3xl mx-auto p-8 lg:px-12 lg:w-3/5">
          <div className="w-full">
            <h1 className="text-3xl font-bold">Let's sign up Comrade</h1>
            <p className="mt-4">
              Lets the Kommissar get you all set up so you can verify your personal account
              and begin setting up your profile.
            </p>

            <div className="grid grid-cols-1 gap-6 mt-8 md:grid-cols-2">
              <div>
                <label className="block mb-2 text-sm font-bold">
                  Full name
                </label>
                <input
                  type="text"
                  name="hoTen"
                  onChange={formik.handleChange}
                  placeholder="John"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">
                  Phone number
                </label>
                <input
                  type="text"
                  name="soDt"
                  onChange={formik.handleChange}
                  placeholder="XXXX-XXX-XXX"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Username</label>
                <input
                  type="text"
                  name="taiKhoan"
                  onChange={formik.handleChange}
                  placeholder="Cristionel Messinaldo"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Password</label>
                <input
                  type="password"
                  name="matKhau"
                  onChange={formik.handleChange}
                  placeholder="Enter your password"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">Email</label>
                <input
                  type="email"
                  name="email"
                  onChange={formik.handleChange}
                  placeholder="siuuuuu@gmail.com"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-bold">
                  Confirm password
                </label>
                <input
                  type="password"
                  placeholder="Enter your password"
                  className="block w-full px-5 py-3 mt-2 text-gray-700 placeholder-gray-400 bg-gray-800 border border-gray-200 rounded-md"
                />
              </div>
              <button className="flex items-center justify-between w-full px-6 py-3 text-sm tracking-wide text-red-700 capitalize transition-colors duration-300 transform bg-yellow-200 rounded-md hover:bg-blue-400 focus:outline-none focus:ring focus:ring-blue-300 focus:ring-opacity-50">
                <span>Join us</span>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="w-5 h-5 rtl:-scale-x-100"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                    clipRule="evenodd"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
    </form>
  );
}
