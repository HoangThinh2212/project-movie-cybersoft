import React from "react";
import Lottie from "lottie-react";
import contact from "../../assets/contact.json";

export default function ContactPage() {
  return (
    <div className="flex flex-wrap relative justify-center items-center h-screen w-screen">
      <h1 className="absolute top-0">Under Construction ...</h1>
      <Lottie
        animationData={contact}
        loop={true}
        style={{ width: "50%", margin: "auto" }}
      />
    </div>
  );
}
