const initialState = {
  carouselImg: [],
};

export const carouselReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SET_CAROUSEL": {
      return { ...state, carouselImg: payload };
    }
    default:
      return state;
  }
};
